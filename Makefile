IMAGE_NAME           = ipmi-exporter
IMAGE_VERSION       ?= 2.0.2
IMAGE_TAG            = trojsten/$(IMAGE_NAME):$(IMAGE_VERSION)

.PHONY: docker docker-clean docker-shell docker-fresh

docker: Dockerfile
	docker build --rm -t $(IMAGE_TAG) -t trojsten/$(IMAGE_NAME):latest .

docker-clean:
	docker rmi -f `docker images -q $(IMAGE_TAG)` || true
	docker rmi -f `docker images -q trojsten/$(IMAGE_NAME):latest` || true

docker-shell: docker
	docker run --rm -it $(IMAGE_TAG) sh

docker-fresh: docker-clean docker
