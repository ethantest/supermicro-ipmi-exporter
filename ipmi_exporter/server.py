import logging
from urllib.parse import urlparse, parse_qs
from socketserver import ThreadingMixIn
from http.server import HTTPServer, SimpleHTTPRequestHandler

from prometheus_client import generate_latest, CollectorRegistry

from .collector import IPMICollector


class ThreadingHTTPServer(HTTPServer, ThreadingMixIn):
    'Threading http server'


class IPMICollectorHandler(SimpleHTTPRequestHandler):
    'HTTP handler for IPMI collecotor'
    logger = logging.getLogger(__name__)

    def __init__(self, command, username, password, *args, **kwargs):
        self.username = username
        self.cmd = command
        self.password = password
        SimpleHTTPRequestHandler.__init__(self, *args, **kwargs)

    def do_GET(self):
        self.logger.info(f'Request from: {self.client_address}')
        url = urlparse(self.path)
        targets = parse_qs(url.query).get('target')
        if url.path == '/metrics':
            if targets is not None:
                for target in targets:
                    self.logger.info(f'Collecting from: {target}')
                    reg = CollectorRegistry(auto_describe=True)
                    reg.register(
                        IPMICollector(
                            self.cmd, target, self.username, self.password
                        )
                    )
                    self.send_response(200)
                    self.end_headers()
                    self.wfile.write(generate_latest(reg))
            else:
                self.send_response(400)
                self.end_headers()
        else:
            self.send_response(404)
            self.end_headers()
