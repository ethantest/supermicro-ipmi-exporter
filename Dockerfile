FROM python:3.7.1-alpine3.8

RUN mkdir -p /home/exporter/ipmi_exporter && apk add --no-cache --update openjdk8-jre-base

WORKDIR /home/exporter
COPY SMCIPMITool.jar /opt/ipmi-exporter/
COPY config.ini.example /etc/ipmi-exporter/config.ini
COPY setup.py SMCIPMITool LICENSE /home/exporter/
COPY ipmi_exporter /home/exporter/ipmi_exporter/
RUN python3 -m pip install .

ENV IPMI_COMMAND="/home/exporter/SMCIPMITool"
CMD ipmi-exporter -c /etc/ipmi-exporter/config.ini
EXPOSE 9920
RUN adduser -h /home/exporter -D exporter
